﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneBehaviour : MonoBehaviour
{
    [SerializeField] float _portance;
    [SerializeField] float _speed;
    [SerializeField] float _enginePower;
    [SerializeField] float _rollSpeed;
    [SerializeField] float _frontRollSpeed;

    [SerializeField] GameObject _fpsCam;

    private Rigidbody rb;
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }



    void Update()
    {

        Inputs();

        // Debug.Log("X = " + transform.eulerAngles.x + "Y = " + transform.eulerAngles.y + "Z = " + transform.eulerAngles.z);

        if (transform.eulerAngles.x > 300 && transform.eulerAngles.x > 1 && transform.eulerAngles.z > 300 && transform.eulerAngles.z < 60)
            _portance = Mathf.Clamp((Mathf.Abs(360 - transform.eulerAngles.x)), 0, 200);
        rb.AddRelativeForce(Vector3.up * CreateLift());

        rb.AddRelativeForce(Vector3.forward * _enginePower, ForceMode.Force);
        rb.angularDrag = Mathf.Clamp(Speed() / 10, 4, 10);
        rb.drag = Mathf.Clamp(Speed() / 20, 1, 3);




        _speed = Speed();


        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (_fpsCam.activeSelf)
                _fpsCam.SetActive(false);
            else
                _fpsCam.SetActive(true);

        }


    }




    public float CreateLift()
    {
        return (_portance * Speed() / 100);
    }

    public float Speed()
    {
        return Mathf.Clamp(Mathf.Abs(rb.velocity.magnitude), 0, 100);
    }

    public void Inputs()
    {
        if (Input.GetKey(KeyCode.Z))
        {

            rb.AddRelativeTorque(Vector3.right * _frontRollSpeed);


        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.AddRelativeTorque(Vector3.left * _frontRollSpeed);


        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.localEulerAngles += Vector3.forward * _rollSpeed;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.localEulerAngles += -Vector3.forward * _rollSpeed;
        }
        else
        {
            //   transform.rotation = Quaternion.Lerp(transform.rotation, new Quaternion(0, 0, 0, 0), 0.9f);
            //   rb.angularVelocity = Vector3.Lerp(rb.angularVelocity, Vector3.zero, 0.1f);
        }
        if (Input.GetMouseButton(0) && _enginePower < 100)
        {
            _enginePower++;
        }
        if (Input.GetMouseButton(1) && _enginePower > 0)
        {
            _enginePower--;
        }


        if (Input.GetKey(KeyCode.Space))
        {
            transform.position = new Vector3(-57.03f, 52.9f, 0);
            transform.rotation = new Quaternion(0, 0, 0, 0);

        }



    }




    IEnumerator resetBalance()
    {
        while (transform.rotation.eulerAngles != Vector3.zero)
        {
            if (transform.rotation.x > 0)
            {

            }
            else if (transform.rotation.x < 0)
            {

            }
        }
        return null;
    }

}
